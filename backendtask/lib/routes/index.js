var express = require('express'),
app = express()
// Load user routes
const userRoutes = require('../user/userRoutes');
// Load response module
const responseHandler = require('../global/Responder');
// Load admin module
// const adminRoutes = require('../admin/adminRoutes');
//========================== Load Modules End ==============================================

//========================== Export Module Start ====== ========================

module.exports = function (app) {
    // Attach User Routes
    app.use('/synapseIndia/api/v1/user', userRoutes);
    // // Attach ErrorHandler to Handle All Errors
    // app.use(responseHandler.apiResponder);
    //   // Attach admin route
    //  app.use('/synapseIndia/api/v1/admin', adminRoutes);
};
