const usrRoutr = require("express").Router();
const resHndlr = require("../global/Responder");
const jwtHandler = require("../jwtHandler");
const userServices = require("./userServices");
const middleware = require("../middleware");
usrRoutr.route("/signup")
    .post([ /*middleware.authenticate.autntctTkn*/ ], function(req, res) {
        userServices.signup(req, res);
    });
usrRoutr.route("/verifyEmail")
    .get([ /*middleware.authenticate.autntctTkn*/ ], function(req, res) {
        userServices.verifyEmail(req, res);
    });
usrRoutr.route("/login")
    .post([ /*middleware.authenticate.autntctTkn*/ ], function(req, res) {
        userServices.login(req, res);
    });
usrRoutr.route("/product")
    .get([ /* middleware.authenticate.autntctTkn*/  ], function(req, res) {
        userServices.productsDetails(req, res);
    });
usrRoutr.route("/makeProduct")
    .post([/* middleware.authenticate.autntctTkn*/ ], function(req, res) {
        userServices.makeProduct(req, res);
    });
usrRoutr.route("/profile")
    .post([ /* middleware.authenticate.autntctTkn*/  ], function(req, res) {
        userServices.profile(req, res);
    });
usrRoutr.route("/addProduct")
    .post([ /* middleware.authenticate.autntctTkn*/  ], function(req, res) {
        userServices.addProduct(req, res);
    });
usrRoutr.route("/changePassword")
    .post([ /* middleware.authenticate.autntctTkn*/  ], function(req, res) {
        userServices.changePassword(req, res);
    });
usrRoutr.route("/forgetPassword")
    .post([ /*middleware.authenticate.autntctTkn*/ ], function(req, res) {
        userServices.forgetPassword(req, res);
    });
module.exports = usrRoutr;