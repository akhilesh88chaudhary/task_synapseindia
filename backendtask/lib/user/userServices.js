const User = require('../user/user');
const Constants = require('./userConstants');
const globalFunction = require('../global/globalFunctions');
const resHndlr = require("../global/Responder");
var bcrypt = require('bcrypt');
const accountSid = 'AC46046362dac8e57a59a867bc54c59f82';
const authToken = 'f32f6dd93e1281d3b5abc9e91cecfa79';
const client = require('twilio')(accountSid, authToken);
var Promise = require("bluebird");
var jwt = Promise.promisifyAll(require("jsonwebtoken"));
var jwtHandlr = require("../jwtHandler");
const nodemailer = require('nodemailer');
const Products = require('./products');
module.exports = {
'signup': (req, res) => { // just a demo api not for use
  console.log("111111111111",req.body)
    bcrypt.genSalt(10, function(err, salt) {
        if (!req.body.email || !req.body.password || !req.body.firstName || !req.body.lastName) 
            resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 400)
        else {
                    bcrypt.hash(req.body.password, salt, function(err, hash) {
                        if (err)
                         return resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 500);
                        else var userData = {
                            email: req.body.email,
                            name: req.body.firstName,
                            password: hash,
                            lastName:req.body.lastName
                        }
                        User.create(userData, function(err, user) {
                            if (err) {
                                if(err.code == 11000 || err.code == "11000")
                                return resHndlr.apiResponder(req, res, Constants.MESSAGES.EmailAlreadyExsist, 400)
                            else
                                return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
                            } else {
                                globalFunction.sendMail(req.body.firstName,user._id,req.body.email, 'subject', 'text', (err, result) => {
                                    if (result) {
                                        return resHndlr.apiResponder(req, res, Constants.MESSAGES.evrifyMail, 200)
                                        
                                    } 
                                    else
                                     resHndlr.apiResponder(req, res, "wrong", 500)
                                })
                            }
                        });
                    })
        }
    })
},
'verifyEmail':(req,res)=>{
    if(!req.query.userId)
    resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 400)
else
{ 
    let date = new Date(new Date().getTime() - 60 * 60 * 24 * 1000);
    console.log('test', date);
    User.findOne({createdAt:{$gte:date},'status':{$ne:'active'},_id:req.query['userId']}) 
    .then((success)=>{
        if(success)
        {
            success.status = 'active';
                success.save(function(err,result)
                {
                    if(err)
                        resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
                    else
                        // resHndlr.apiResponder(req, res, 'Email verified successfully.', 200)
                    return res.redirect('http://localhost:4200/login');
                })
        }
        else
        {
            resHndlr.apiResponder(req, res, 'Link expired or you have already verified your email id.', 400)
        }
    })
    .catch((unsuccess)=>{
        resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
    })
}
},
'login': (req, res) => {

        if(!req.body.email || !req.body.password)
          resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 400)
          else  
        User.findOne({email: req.body.email}).lean().exec(function(err, record) {
            if (err)
                return resHndlr.apiResponder(req, res, Constants.MESSAGES.PasswordMismatch, 400)
            else if (record) {
                if (record.status == 'active' || record.status == 'Active')
                    bcrypt.compare(req.body.password, record.password, (err, result) => {
                        if (result) {
                           var token = jwtHandlr.genUsrToken({id: record._id});
                            record.token = token
                            return resHndlr.apiResponder(req, res, 'Successfully logged in.', 200, record)
                        } else {
                            return resHndlr.apiResponder(req, res, Constants.MESSAGES.PasswordMismatch, 400)
                        }
                    })
                else
                    return resHndlr.apiResponder(req, res, 'Please verify your account', 400)
            } else
                return resHndlr.apiResponder(req, res, Constants.MESSAGES.EmailNotExsist, 400)
        })
},
'productsDetails':(req,res)=>{
  Products.find()
    .then((success)=>{
      return resHndlr.apiResponder(req, res, 'Product list.', 200, success)
    })
    .catch((unsuccess)=>{
      return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
    })
},
'makeProduct':(req,res)=>{
  Products.create(req.body)
    .then((success)=>{
      return resHndlr.apiResponder(req, res, 'You have successfully listed a new product.', 200, success)
    })
    .catch((unsuccess)=>{
      return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
    })
},
'addProduct':(req,res)=>{
  if(req.body.products.length<1)
        return resHndlr.apiResponder(req, res, Constants.MESSAGES.productReq, 400)
  User.update({'_id':req.body.userId},{$push:{'myProducts':{ $each: req.body.products }}},{new:true})
    .then((success)=>{
      return resHndlr.apiResponder(req, res, 'You successfully added products in my products.', 200, success)
    })
    .catch((unsuccess)=>{
      return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
    })
},
'profile':(req,res)=>{
    if(!req.body.userId)
        resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 400)
    else
        User.findOne({_id:req.body.userId}).populate('myProducts').lean().exec()
    .then((success)=>{
        if(success)
            delete success.password
        return resHndlr.apiResponder(req, res, 'Your profile.', 200, success)
    })
    .catch((unsuccess)=>{
            resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
        })
},
'changePassword': (req, res) => {
    if (!req.body._id || !req.body.oldPassword || !req.body.newPassword)
     return resHndlr.apiResponder(req, res, Constants.MESSAGES.RequiredField, 400)
    else User.findOne({
        _id: req.body._id
    }).then((success) => {
        bcrypt.compare(req.body.oldPassword, success.password, function(err, isMatch) {
            if (err) resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
            else
            if (isMatch) bcrypt.genSalt(10, function(err, salt) {
                bcrypt.hash(req.body.newPassword, salt, function(err, hash) {
                    if (err) return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
                    else {
                        success.password = hash;
                        success.save(function(err, result) {
                            if (err) return resHndlr.apiResponder(req, res, Constants.MESSAGES.SomeThingWrong, 400)
                            else return resHndlr.apiResponder(req, res, 'Your password updated successfully.', 200)
                        })
                    }
                });
            })
            else return resHndlr.apiResponder(req, res, 'Please provide correct password.', 400)
        });
    })
},
'forgetPassword': function(req, res) {
    User.findOne({email: req.query.email}, (err, record) => {
        if (err)
            return resHndlr.apiResponder(req, res, 'Something went wrong.', 400)
        if (record) {
            var text = '';
            var otppossible = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            for (var i = 0; i < 8; i++) {
                text += otppossible.charAt(Math.floor(Math.random() * otppossible.length));
            };
            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: "synapseIndiatech@gmail.com",
                    pass: "synapseIndia@20!8"
                }
            });
            var mailOptions = {
                from: 'synapseIndia',
                to: req.query.email,
                subject: 'Temporary password for synapseIndia.',
                html: `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
            <head>
              <meta name="viewport" content="width=device-width, initial-scale=1.0" />
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
              <title>Set up a new password for [Product Name]</title>


            </head>
            <body style="-webkit-text-size-adjust: none; box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; height: 100%; line-height: 1.4; margin: 0; width: 100% !important;" bgcolor="#F2F4F6"><style type="text/css">
          body {
          width: 100% !important; height: 100%; margin: 0; line-height: 1.4; background-color: #F2F4F6; color: #74787E; -webkit-text-size-adjust: none;
          }
          @media only screen and (max-width: 600px) {
            .email-body_inner {
              width: 100% !important;
            }
            .email-footer {
              width: 100% !important;
            }
          }
          @media only screen and (max-width: 500px) {
            .button {
              width: 100% !important;
            }
          }
          </style>
              <span class="preheader" style="box-sizing: border-box; display: none !important; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 1px; line-height: 1px; max-height: 0; max-width: 0; mso-hide: all; opacity: 0; overflow: hidden; visibility: hidden;">Use this link to reset your password. The link is only valid for 24 hours.</span>
              <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%;" bgcolor="#F2F4F6">
                <tr>
                  <td align="center" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word;">
                    <table class="email-content" width="100%" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%;">


                      <tr>
                        <td class="email-body" width="100%" cellpadding="0" cellspacing="0" style="-premailer-cellpadding: 0; -premailer-cellspacing: 0; border-bottom-color: #EDEFF2; border-bottom-style: solid; border-bottom-width: 1px; border-top-color: #EDEFF2; border-top-style: solid; border-top-width: 1px; box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0; padding: 0; width: 100%; word-break: break-word;" bgcolor="#FFFFFF">
                          <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0 auto; padding: 0; width: 570px;" bgcolor="#FFFFFF">

                            <tr>
                              <td class="content-cell" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; padding: 35px; word-break: break-word;">
                                <h1 style="box-sizing: border-box; color: #2F3133; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 19px; font-weight: bold; margin-top: 0;" align="left">Hi,</h1>
                                <p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; line-height: 1.5em; margin-top: 0;" align="left">You recently requested to forgot your password for your synapseIndia account. Use the Temporary password below to reset it. <strong style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;"></strong></p>

                                <table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 30px auto; padding: 0; text-align: center; width: 100%;">
                                  <tr>
                                    <td align="center" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word;">

                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                        <tr>
                                          <td align="center" style="box-sizing: border-box; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; word-break: break-word;">
                                             <h5 style="box-sizing: border-box; color: #2F3133; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 15px; font-weight: bold; margin-top: 0;" align="left">${text}</h5>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                                <p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; line-height: 1.5em; margin-top: 0;" align="left">Thanks,
                                <br />The synapseIndia Team</p>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </body>
          </html>`
            };
            transporter.sendMail(mailOptions, function(error, info) {
                if (error) 
                         return resHndlr.apiResponder(req, res, 'Something went wrong.', 400)
                 else {
                    bcrypt.hash(text, 9, function(err, hash) {
                        if (err)
                         return resHndlr.apiResponder(req, res, 'Something went wrong.', 400)
                        else User.update({email: req.query.email}, {password: hash}, {new: true}, function(err, record) {
                            if (err) 
                         return resHndlr.apiResponder(req, res, 'Something went wrong.', 400)
                         return resHndlr.apiResponder(req, res, 'Your temporary password is send on your email.', 200)
                        });
                    });
                }
            });
        }
        else
        {
            return resHndlr.apiResponder(req, res, 'Sorry this email id is not registered.', 400)
        }
    })
 }
}
