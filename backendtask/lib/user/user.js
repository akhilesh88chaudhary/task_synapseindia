const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let user = new Schema({
firstName:{type:String},
lastName:{type:String},
socialId:{type:String},//social id
status:{type:String,default:"deactive"},
email:{type:String,trim: true,lowercase: true,unique: true},
password:{type:String}, 
createdAt:{type:Date, default:Date.now},
token:{
	status:{type:String},
	token_id:{type:String}
},
role:{type:String,default:"user"},
myProducts:[{type:String,ref:'product',sparse:true}]
});

let adminEntry = mongoose.model('User',user);

module.exports = mongoose.model('User',user);

var invoke = function()
{
	adminEntry.count(function(err,data)
	{
		
		if(err)
		{
			console.log("Please try again later.");
		}
		else if(data!=0)
		{
			console.log("Super admin service start...");
		}
		else
		{
		adminEntry = new adminEntry({'state.status':'active','role':'superadmin'});//later u can save email and pass accordingly
			adminEntry.save(function(err,data)
			{
				if(err)
				{
					console.log(err);
					console.log("Error in saving Admin.");
				}
				else
				{
					console.log("SuperAdmin created successfully.");
				}
			})
		}
	})
}
invoke();