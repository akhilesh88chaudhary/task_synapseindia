let verificationURL = '';
const MESSAGES = {
    NAME_CANT_EMPTY: "Name is empty or invalid.",
    platformCantEmpty : "Platform cannot be empty or invalid.",
    DeviceTokenCantEmpty:"DeviceToken cannot be empty or invalid.",
    EmailCantEmpty:"Email id cannot be empty.",
    InvalidEmail:"Invalid email.",
    validationError : "Validation errors.",
    UserIdCantEmpty : "User id cannot be empty.",
    RequiredField : "Please fill all the required fields.",
    EmailAlreadyExsist: "This email id is already registered.",
    SomeThingWrong : "Something went wrong.",
    Limit:5,
    Data:'Your requested data.',
    productReq:'Please select atleast one product.',
    EmailNotExsist:'This email is not exist.',
    PasswordMismatch: 'Password mismatch.',
    evrifyMail:'You have signed up successfully. For verification, an email has been sent to you along with activation link over your mail.Please activate your account by clicking on link.',
    SignupRule: 'New Signups are not allowed for sometime, appreciate your patience.'
  }
  module.exports = {
    MESSAGES:MESSAGES
};