import { Component, OnInit } from '@angular/core';
import { ApiIntergrationService } from '../api-intergration.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-my-product',
  templateUrl: './my-product.component.html',
  styleUrls: ['./my-product.component.css']
})
export class MyProductComponent implements OnInit {
public productList : any;
userData:any;
  constructor(
    private api:ApiIntergrationService,
    private router: Router
  ) { }
  productListApi(){
    var data ={
      userId:this.userData._id
    }
    console.log(data)
    this.api.profile(data).subscribe((response) => {
   if (response['responseCode']== 200) {
     console.log("response['data']",response['data'])
    this.productList = response['data'].myProducts
    } else {
       alert(response['responseMessage']);
    }
  });
  }
  logout(){
    localStorage.clear();
    this.router.navigate(['/login']);

  }
  ngOnInit() {

    this.productList = []
    this.userData = JSON.parse(localStorage.getItem('id'))
      this.productListApi()
  }

}
