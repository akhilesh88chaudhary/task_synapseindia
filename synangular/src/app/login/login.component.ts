import { Component, OnInit } from '@angular/core';
import { FormsModule, FormControl, FormBuilder, Validators, FormGroup, ReactiveFormsModule, } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ApiIntergrationService } from '../api-intergration.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private api:ApiIntergrationService,
    private toastr: ToastrService
  ) { }
  login(){
    this.api.login(this.loginForm.value).subscribe((response) => {
   if (response['responseCode']== 200) {
      alert(response['responseMessage']);
      localStorage.setItem('id',JSON.stringify(response['data']))
      this.router.navigate(['/product']);
    } else {
       alert(response['message']);
    }
  });
  //  this.router.navigate(['/product']);

  }
  loginFormInit(){
            this.loginForm = this.fb.group({
                   email : new FormControl('',Validators.compose([Validators.required,Validators.pattern(/^[a-zA-Z][-_.a-zA-Z0-9]{2,29}\@((\[[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,15}|[0-9]{1,3})(\]?)$/)])),
                   password : new FormControl('',Validators.required),
                   ranNum : new FormControl('',Validators.required),
                   random : new FormControl('',Validators.required)

              });
          }
  ngOnInit() {
    this.loginFormInit()
  }

}
