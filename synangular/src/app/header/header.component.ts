import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
public userData:any;
  constructor(
    private router: Router
  ) { }
product(){
  if(this.userData){
  this.router.navigate(['/myProduct']);
}else
alert("Please login first.")

}

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem('id'))
  }

}
