import { Component, OnInit } from '@angular/core';
import { FormsModule, FormControl, FormBuilder, Validators, FormGroup, ReactiveFormsModule, } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ApiIntergrationService } from '../api-intergration.service';
declare var $;
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  productForm: FormGroup;
  productList:any;
  userData:any;
  myProduct:any=[];
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private api:ApiIntergrationService
  ) {
  }
  product(){
    this.api.makeProduct(this.productForm.value).subscribe((response) => {
   if (response['responseCode']== 200) {
     alert(response['responseMessage'])
     $('#myModal').modal('hide');
     this.productListApi()
      // localStorage.setItem('id',JSON.stringify(response['result']))
    //this.router.navigate(['/product']);
    } else {
       alert(response['responseMessage']);
    }
  });
  }
  addToCart(){
    var data = {
      products:this.myProduct,
      userId:this.userData._id
    }
    console.log("data",data)
    this.api.addProduct(data).subscribe((response) => {
   if (response['responseCode']== 200) {
     alert(response['responseMessage'])

      // localStorage.setItem('id',JSON.stringify(response['result']))
    this.router.navigate(['/myProduct']);
    } else {
       alert(response['responseMessage']);
    }
  });
  }
  logout(){
    localStorage.clear();
    this.router.navigate(['/login']);

  }
  productListApi(){
    this.api.productDetail().subscribe((response) => {
   if (response['responseCode']== 200) {
     console.log("response['data']",response['data'])
    this.productList = response['data']
    } else {
       alert(response['responseMessage']);
    }
  });
  }
  select(id){
    if(this.myProduct.includes(id))
      this.myProduct.splice(this.myProduct.indexOf(id),1)
    else
    this.myProduct.push(id)

    console.log(this.myProduct)
  }
  productFormInit(){
  this.productForm = this.fb.group({
         productName : new FormControl('',Validators.compose([Validators.required,Validators.pattern(/^[a-zA-Z ]{2,30}$/)])),
         price : new FormControl('',Validators.compose([Validators.required,Validators.pattern(/^[0-9]/)])),
         description: new FormControl('',Validators.required)
    });
}
  ngOnInit() {
    this.productFormInit();
    this.productListApi()
    this.userData = JSON.parse(localStorage.getItem('id'))
  }

}
