import { Component, OnInit } from '@angular/core';
import { FormsModule, FormControl, FormBuilder, Validators, FormGroup, ReactiveFormsModule, } from '@angular/forms';
import { ApiIntergrationService } from '../api-intergration.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private api:ApiIntergrationService
  ) { }

  signup(){
    this.api.signup(this.signupForm.value).subscribe((response) => {
   if (response['responseCode']== 200) {
     alert(response['responseMessage'])
      // localStorage.setItem('id',JSON.stringify(response['result']))
    this.router.navigate(['/login']);
    } else {
       alert(response['responseMessage']);
    }
  });
  }
  signupFormInit(){
  this.signupForm = this.fb.group({
         email : new FormControl('',Validators.compose([Validators.required,Validators.pattern(/^[a-zA-Z][-_.a-zA-Z0-9]{2,29}\@((\[[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,15}|[0-9]{1,3})(\]?)$/)])),
         password : new FormControl('',Validators.compose([Validators.required,Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9!@#$%^&*]{6,16}$/)])),
         firstName : new FormControl('',Validators.compose([Validators.required,Validators.pattern(/^[a-zA-Z ]{2,30}$/)])),
         lastName : new FormControl('',Validators.compose([Validators.required,Validators.pattern(/^[a-zA-Z ]{2,30}$/)])),
         Cpassword:new FormControl('',Validators.required)
    });
}

  ngOnInit() {
    this.signupFormInit()
  }

}
