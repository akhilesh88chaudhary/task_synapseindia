import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ApiIntergrationService {
url:any = "http://localhost:5000/synapseIndia/api/v1/user"
  constructor(private http:HttpClient) { }

  login(postData){
  return this.http.post(this.url+"/login", postData)
  }
  signup(postData){
  return this.http.post(this.url+"/signup", postData)
  }
  makeProduct(postData){
  return this.http.post(this.url+"/makeProduct", postData)
  }
  productDetail(){
  return this.http.get(this.url+"/product")
  }
  addProduct(postData){
  return this.http.post(this.url+"/addProduct",postData)
  }
  profile(postData){
  return this.http.post(this.url+"/profile",postData)
  }
}
